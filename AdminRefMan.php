<?php
require_once _PS_ADMIN_DIR_ . '/../classes/AdminTab.php';
class AdminRefMan extends AdminTabCore {
	private $_html = '';
	public $className = '';
	/**
	 *
	 * @var refman
	 */
	public $mod;
	/**
	 *
	 * @var DbCore
	 */
	public $db;
	public $refs, $ref;
	public $errors, $img = array ();
	private $imgsCnt = 0;
	public function __construct() {
		$this->className = basename ( __FILE__, '.php' );
		parent::__construct ();
		$this->mod = Module::getInstanceByName ( 'refman' );
		$this->db = Db::getInstance ();
		
		$this->tab_id = Tools::getValue ( 'tab_id', '1' );
		
		$this->usetitle = Configuration::get ( "{$this->mod->name}_usetitle" );
		$this->useref = Configuration::get ( "{$this->mod->name}_useref" );		
	}
	
	public function addError($str) {
		$this->mod->addError ( $str );
	}
	public function printErrors($return = false) {
		return $this->mod->printErrors ( $return );
	}
	private function treatErrors() {
		return $this->mod->hasErrors () ? '<div class="error">' . $this->mod->printErrors ( true ) . '</div>' : '';
	}
	public function display() {
		$sql = "SELECT DISTINCT({$this->mod->dbfield}) as str FROM " . _DB_PREFIX_ . "product ORDER BY {$this->mod->dbfield};";
		$refs = $this->db->executeS ( $sql );
		
		$options = '<datalist id="references">';
		foreach ( $refs as $ref ) {
			$options .= '<option value="' . $ref ['str'] . '">';
		}
		$options .= '</datalist>'."\r\n";

		$sql4 = "SELECT DISTINCT(value) as str FROM " . _DB_PREFIX_ . "crosslinks ORDER BY str ASC;";
		$refs4 = $this->db->executeS ( $sql4 );
		
		$options .= '<datalist id="references4">';
		foreach ( $refs4 as $ref ) {
			$options .= '<option value="' . $ref ['str'] . '">';
		}
		$options .= '</datalist>';
		
		$this->_html .= '<center> ' . nl2br ( $this->treatErrors () ) . ' </center>
    ' . $options . '
		<ul id="menuTab">
    <li id="menuTab1" class="menuTabButton selected">1. ' . $this->l ( 'Tags editor' ) . '</li>
    <li id="menuTab2" class="menuTabButton selected">2. ' . $this->l ( 'Images' ) . '</li>
    <li id="menuTab3" class="menuTabButton selected">3. ' . $this->l ( 'Descriptions' ) . '</li>
    <li id="menuTab4" class="menuTabButton selected">4. ' . $this->l ( 'Cross links' ) . '</li>
    </ul>
    <div id="tabList">
    <div id="menuTab1Sheet" class="tabItem selected">' . $this->_displayTagsForm () . '</div>
    <div id="menuTab2Sheet" class="tabItem selected">' . $this->_displayImagesForm () . '</div>
    <div id="menuTab3Sheet" class="tabItem selected">' . $this->_displayDescsForm () . '</div>
    <div id="menuTab4Sheet" class="tabItem selected">' . $this->_displayCLinksForm () . '</div>
    </div>' . "\r\n";
		
		echo $this->_html;
	}
	private function _displayTagsForm() {
		$this->ref = Tools::getValue ( 'ref', '' );
		$strTags = Tools::getValue ( 'strTags', '' );
		
		$usetitle = Tools::getValue ( "usetitle", "off" );
		$useref = Tools::getValue ( "useref", "off" );
		$usesupref = Tools::getValue ( "usesupref", "off" );
		
		$add2pos = Tools::getValue ( 'add2pos', 'append' );
		$add2mk = Tools::getValue ( 'add2mk', '' );
		$add2tags = Tools::getValue ( 'add2tags', '' );
		
		$agentitle = Tools::getValue('agen-title',Tools::isSubmit ( 'submitQuery' )?"0":"1");
		$agendesc = Tools::getValue('agen-desc',Tools::isSubmit ( 'submitQuery' )?"0":"1");
		$agenrewrite = Tools::getValue('agen-rewrite',Tools::isSubmit ( 'submitQuery' )?"0":"1");
		
		$html = '<fieldset class="width4"><legend><img size="24" src="../img/t/' . $this->className . '.gif" />' . $this->l ( 'Set Tags by reference' ) . '</legend>
    <table class="searchSEO"><tr><td>
    ' . $this->l ( 'Reference' ) . ':</td><td><input list="references" name="ref" value="' . $this->ref . '" /></td></tr><tr><td>
    ' . $this->l ( 'Keywords separate by commas' ) . ':</td><td> <input type="text" id="strTags" name="strTags" size="60" value="' . $strTags . '" /></td></tr><tr><td>
    ' . $this->l ( 'Insert keywords at ' ) . '</td><td><select name="add2pos"><option value="prepend"' . ($add2pos == 'prepend' ? 'selected="selected"' : '') . '>' . $this->l ( 'Prepend' ) . '</option><option value="append"' . ($add2pos == 'append' ? 'selected="selected"' : '') . '>' . $this->l ( 'Append' ) . '</option></select>
    </td></tr><tr><td>
    ' . $this->l ( 'Add to fields' ) . '</td><td><input type="checkbox" name="add2mk" value="in"' . ($add2mk == 'in' ? ' checked="checked"' : '') . '> ' . $this->l ( 'Meta keywords' ) . '
    <input type="checkbox" name="add2tags" value="in"' . ($add2tags == 'in' ? ' checked="checked"' : '') . '> ' . $this->l ( 'Tags' ) . '</td></tr>
    <tr><td><input type="checkbox" name="usetitle" value="on" ' . ($usetitle == 'on' ? 'checked ' : '') . '/></td><td>' . $this->l ( 'Use parsed title as keywords' ) . '</td></tr>
    <tr><td><input type="checkbox" name="usesupref" value="on" ' . ($usesupref == 'on' ? 'checked ' : '') . '/></td><td>' . $this->l ( 'Ad the supplier reference field as keywords' ) . '</td></tr>
    <tr><td><input type="checkbox" name="useref" value="on" ' . ($useref == 'on' ? 'checked ' : '') . '/></td><td>' . $this->l ( 'Ad the shop reference field as keywords' ) . '</td></tr>
    </table>
    <fieldset><legend>'.$this->l('Activer l\'auto-génération pour les champs suivant').':</legend>
    	<input type="checkbox" name="agen-title" value="1" '.($agentitle=="1"?'checked="checked"':'').' /> '.$this->l('Meta title').'	
    	<input type="checkbox" name="agen-rewrite" value="1" '.($agenrewrite=="1"?'checked="checked"':'').' /> '.$this->l('Link rewrite').'	
    	<input type="checkbox" name="agen-desc" value="1" '.($agendesc=="1"?'checked="checked"':'').' /> '.$this->l('Meta description').'	
    </fieldset>
    <input type="submit" name="submitQuery" value="' . $this->l ( 'Prepare Query' ) . '" />
    </fieldset>';
		
		if (Tools::isSubmit ( 'submitQuery' )) {
			if (is_array ( $this->refs ) && (count ( $this->refs ) > 0)) {
				$datas = '';
				foreach ( $this->refs as $id_prod => $strs ) {
					$datas .= '<table class="prodsSEO"><tr><td>' 
							. $this->l ( 'Product name' ) . ':</td><td style="background-color:#ddd; font-weight: bold;">' . $strs ['name'] . '</td></tr>' 
							. '<tr><td>' . $this->l ( 'Link rewrite' ) . ':</td><td> <input type="text" name="prods[' . $id_prod . '][link_rewrite]" value="' . ($agenrewrite=="1"?$strs ['link_rewrite']:$strs['unmodified']['link_rewrite']) . '" /></td></tr>' 
							. '<tr><td>' . $this->l ( 'Meta title' ) . ':</td><td> <input type="text" name="prods[' . $id_prod . '][meta_title]" value="' . ($agentitle=="1"?$strs ['meta_title']:$strs['unmodified']['meta_title']) . '" /></td></tr>' 
							. '<tr><td>' . $this->l ( 'Meta keywords' ) . ':</td><td> <input type="text" name="prods[' . $id_prod . '][meta_keywords]" value="' . $strs ['meta_keywords'] . '" /></td></tr>' 
							. '<tr><td>' . $this->l ( 'Meta description' ) . ':</td><td> <input type="text" name="prods[' . $id_prod . '][meta_description]" value="' . ($agendesc=="1"?$strs['meta_description']:$strs['unmodified']['meta_description']) . '" /></td></tr>' 
							. '<tr><td>' . $this->l ( 'Tags' ) . ':</td><td> <textarea name="prods[' . $id_prod . '][tags]" rows="5">' . $strs ['tags'] . '</textarea><br/></td></tr>' 
							. '</table><br/>';
				}
				
				$html .= '<fieldset class="width4"><legend><img size="24" src="../img/t/' . $this->className . '.gif" />' . $this->l ( 'Tags & meta for reference' ) . ' ' . $this->ref . '</legend>' . $datas . '
        <input type="hidden" name="reference" value="' . $this->ref . '" />
        <input type="submit" name="submitTags" value="' . $this->l ( 'Update Database' ) . '" />
        </fieldset>';
			} else
				$this->errors [] = $this->l ( 'No product found for this query!' );
		}
		return '<center><form action="?' . $this->mod->controller . '=AdminRefMan&module=' . $this->mod->name . '&tab_id=1&token=' . $this->mod->token . '" method="post">' . $html . '</form></center>';
	}
	private function _displayImagesForm() {
		$html_img = '<span style="float: right;">';
		if (count ( $this->img ) > 0) {
			for($i = 0; $i < count ( $this->img ); $i ++)
				$html_img .= '<b>' . ($i == 0 ? $this->l ( 'Cover' ) : $i) . '.</b><br/><img src="' . $this->img [$i] . '"/><br/>';
		}
		$html_img .= '</span>';
		
		$html = '<fieldset class="width4"><legend><img size="24" src="../img/t/' . $this->className . '.gif" />' . $this->l ( 'Set prestashop image' ) . '</legend>
    ' . $this->l ( 'Reference' ) . ':</td><td><input list="references" name="ref" value="' . $this->ref . '" />&nbsp;&nbsp;<input type="submit" name="displayImages" value="' . $this->l ( 'Display' ) . '" /><br/><br/>' . $html_img . '<table width="380">' . '<tr><td>' . $this->l ( 'Cover url : ' ) . '</td><td><input type="text" name="cover_url" value="" size="48" /></td></tr>';
		for($i = 1; $i <= $this->mod->numfields; $i ++)
			$html .= '<tr><td>' . $this->l ( 'Image url ' ) . $i . ' : </td><td><input type="text" name="img_url[' . $i . ']" value="" size="44" /></td></tr>';
		
		$html .= '</table><br/><br/><input type="Submit" name="submitImages" /></fieldset>';
		return '<center><form action="?' . $this->mod->controller . '=AdminRefMan&module=' . $this->mod->name . '&tab_id=2&token=' . $this->mod->token . '" method="post">' . $html . '</form></center>';
	}
	private function _displayDescsForm() {
		$html = '<fieldset class="width4"><legend><img size="24" src="../img/t/' . $this->className . '.gif" />' . $this->l ( 'Set prestashop descriptions' ) . '</legend>
    	' . $this->l ( 'Reference' ) . ': <input list="references" name="ref" value="' . $this->ref . '" />&nbsp;&nbsp;'
    	.'<input type="submit" name="displayDescs" value="' . $this->l ( 'Display' ) . '" /><br/><br/>'
    	.'<b>'.$this->l ( 'Parameters using %parm1% ... %parm4% ' ).'</b><br/>'."\r\n"
    	. $this->l ( 'Short description' ) . ': <textarea class="rte" name="description_short">' . Tools::getValue ( 'description_short' ) . '</textarea><br/>' . $this->l ( 'Description' ) . ': <textarea class="rte" name="description">' . Tools::getValue ( 'description' ) . '</textarea><br/>';
		
		if (Tools::isSubmit ( 'displayDescs' )) {
			if (is_array ( $this->refs ) && (count ( $this->refs ) > 0)) {
				$datas =  '<input type="Submit" name="submitDescs" />'.'<table class="prodsSEO"><tr><td> <input type="checkbox" name="checkAllDescs" value="1" onclick="orefman.checkAllDescs();" />' . '</td><td><span>&lt;' . $this->l ( 'Check All' ) . '</span><span style="width: 500px; text-align:center; display: inline-block;">' . $this->l ( 'Product name' ) . '</span></td></tr>';
				foreach ( $this->refs as $id_prod => $strs ) {
					$datas .= '<tr><td>' . '<input type="checkbox" id="prodchosen-' . $id_prod . '" name="prods[' . $id_prod . ']" value="' . $id_prod . '" onclick="orefman.changeVisibleDesc(' . $id_prod . ',this.checked);" /></td>' 
							. '<td style="background-color:#ddd; font-weight: bold;"> (' . $id_prod . ') <span class="titledesc" onclick="orefman.changeVisibleDesc(' . $id_prod . ');">' . $strs ['name'] . '</td></tr>' 
							. '<tr class="desctr" id="desc-' . $id_prod . '"><td colspan="2">' 
							. $this->l ( 'Parameter 1' ) . ': <input type="button" value="'.$this->l('Insert last parameter').'" onclick=\'$("textarea[name=\\"parms1['.$id_prod.']\\"]").val(orefman.lastParms.parm1);\' /><br/><textarea onBlur="orefman.blurTextArea(this,\'parm1\');" name="parms1['.$id_prod.']" class="rm_descsh"></textarea><br/>'
							. $this->l ( 'Parameter 2' ) . ': <input type="button" value="'.$this->l('Insert last parameter').'" onclick=\'$("textarea[name=\\"parms2['.$id_prod.']\\"]").val(orefman.lastParms.parm2);\' /><br/><textarea onBlur="orefman.blurTextArea(this,\'parm2\');" name="parms2['.$id_prod.']" class="rm_descsh"></textarea><br/>'
							. $this->l ( 'Parameter 3' ) . ': <input type="button" value="'.$this->l('Insert last parameter').'" onclick=\'$("textarea[name=\\"parms3['.$id_prod.']\\"]").val(orefman.lastParms.parm3);\' /><br/><textarea onBlur="orefman.blurTextArea(this,\'parm3\');" name="parms3['.$id_prod.']" class="rm_descsh"></textarea><br/>'
							. $this->l ( 'Parameter 4' ) . ': <input type="button" value="'.$this->l('Insert last parameter').'" onclick=\'$("textarea[name=\\"parms4['.$id_prod.']\\"]").val(orefman.lastParms.parm4);\' /><br/><textarea onBlur="orefman.blurTextArea(this,\'parm4\');" name="parms4['.$id_prod.']" class="rm_descsh"></textarea><br/>'
							.'</td></tr>';
				}
				$datas .= '</table><br/>' . '<input type="Submit" name="submitDescs" />';
				$html .= $datas;
			}
		} 
		if (Tools::isSubmit ( 'submitDescs' )) {
			if (is_array ( $this->refs ) && (count ( $this->refs ) > 0)) {
				$datas = '<table class="prodsSEO">';
				foreach ( $this->refs as $id_prod => $strs ) {
					$datas .= '<tr><td style="background-color:#ddd; font-weight: bold;">  ' . $strs ['name'] . ' => ' . $strs ['result'] . '</td></tr>';
				}
				$datas .= '</table><br/>';
			}
		}
		
		$html .= '</table><br/></fieldset>';
		return '<center><form action="?' . $this->mod->controller . '=AdminRefMan&module=' . $this->mod->name . '&tab_id=3&token=' . $this->mod->token . '" method="post">' . $html . '</form></center>';
	}

	private function _displayCLinksForm() {
		$clinks = '';

		foreach($this->refs as $id_product => $product) {
			
			$i = 0;
			$clinks .= '<tr><td>'.$id_product.'</td>'
					.'<td id="product_'.$id_product.'"><span style="float: right;"><input type="button" name="addition" value="'.$this->l('Add a new').'" onclick="orefman.addCrossLink('.$id_product.');" /></span>'.$product['name'].'<br style="clear: both;" />';
			foreach($product['crosslinks'] as $id_link => $clink) {
				$clinks .= '<div id="link_'.$id_link.'" class="'.($i % 2==0?'evenCLLine':'oddCLLine').'">'; 
			  $clinks .= '<input type="hidden" name="id_link['.$id_link.']" value="'.$id_link.'" />';
			  $clinks .= '<input type="hidden" name="id_product_src['.$id_link.']" value="'.$clink['id_product_src'].'" />';
			  $clinks .= $this->l('Product link id').'<input size="8" type="text" name="id_product_dst['.$id_link.']" value="'.$clink['id_product_dst'].'" />';
			  $clinks .= $this->l('Property name').'<input list="references4"  type="text" name="value['.$id_link.']" value="'.$clink['value'].'" />';
			  $clinks .= '<input type="button" name="deletion" value="'.$this->l('Delete').'" onclick="orefman.deleteCrossLink('.$id_link.');" /><br/></div>';
			  $i++;			  	
			}
				$clinks .= '</td></tr>';
		}
		$html = '<fieldset class="width4"><legend><img size="24" src="../img/t/' . $this->className . '.gif" />' . $this->l ( 'Crosslinks' ) . '</legend>'
    . $this->l ( 'Reference' ) . ': <input list="references" name="ref" value="' . $this->ref . '" />'
    .'<input type="submit" name="submitSearch" value="'.$this->l('search').'" />'
		.'<table class="searchSEO">'
    .$clinks
    .'</table>'
    .(Tools::isSubmit('submitSearch') || ($i>0)?'<input type="submit" name="submitLinks" value="'.$this->l('Update').'" />':'');

		return '<center><form action="?' . $this->mod->controller . '=AdminRefMan&module=' . $this->mod->name . '&tab_id=4&token=' . $this->mod->token . '" method="post">' . $html . '</form></center>';
	}
	
	private function sanitizeKeywords($sample, $kws) {
		$supkws = '';
		$supkwsarr = explode ( ',', $sample );
		$supkwsarrout = array ();
		if (is_array ( $supkwsarr ) && (count ( $supkwsarr ) > 0)) {
			foreach ( $supkwsarr as $kw ) {
				if (! preg_match ( "#(?:[\s\t\n\r]+|,[\s\t\n\r]+|,|^)(" . str_replace ( '#', '\#', $kw ) . ")(?:[\s\t\n\r]+|,[\s\t\n\r]+|,|$)#umis", $kws, $m )) {
					if (! empty ( $kw ) && ! in_array ( $kw, $supkwsarrout ))
						$supkwsarrout [] = $kw;
				}
			}
			$supkws = implode ( ',', $supkwsarrout );
		}
		return $supkws;
	}
	
	public function postProcess() {
		$id_lang = $this->mod->context->language->id;
		$wordsfilter = $this->mod->excwords;
		
		if ($this->tab_id == 1) {
			
			/* Tags tab process */
			$this->ref = Tools::getValue ( 'ref', '' );
			
			$usetitle = Tools::getValue ( 'usetitle', 'off' );
			$usesupref = Tools::getValue ( 'usesupref', 'off' );
			$useref = Tools::getValue ( 'useref', 'off' );
			
			$add2pos = Tools::getValue ( 'add2pos', 'append' );
			$add2mk = Tools::getValue ( 'add2mk', '' );
			$add2tags = Tools::getValue ( 'add2tags', '' );

			$this->db = Db::getInstance ();
			
			if (Tools::isSubmit ( 'submitQuery' )) {
				$sql = "SELECT p.id_product, p.reference, p.supplier_reference, pl.name, pl.meta_keywords, pl.meta_title, pl.link_rewrite, pl.meta_description FROM " . _DB_PREFIX_ . "product AS p JOIN " . _DB_PREFIX_ . "product_lang AS pl ON p.id_product = pl.id_product LEFT JOIN " . _DB_PREFIX_ . "product_attribute pa ON p.id_product = pa.id_product WHERE pl.id_lang = {$this->mod->id_lang} AND (p.{$this->mod->dbfield} LIKE '{$this->ref}' OR pa.{$this->mod->dbfield} LIKE '{$this->ref}');";
				$prods = $this->db->executeS ( $sql );
				$this->refs = array ();
				foreach ( $prods as $prod ) {
					$prod ['attrs'] = '';
					$prod['unmodified'] = array('meta_title' => $prod['meta_title'],'link_rewrite' => $prod['link_rewrite'],'meta_description' => $prod['meta_description']);
					$supkws = Tools::getValue ( 'strTags', '' );
					$meta_kws = explode ( ',', $prod ['meta_keywords'] );
					$prod ['meta_keywords'] = '';
					foreach ( $meta_kws as $kw ) {
						$kw = trim($kw, ', ');
						if (preg_match ( '#((^|,)' . preg_quote ( $kw, '#' ) . '(,|$))#', $prod ['meta_keywords'], $m ) == 0)
							$prod ['meta_keywords'] .= $kw . ',';
					}
					
					$attrs = $this->db->executeS ( 'SELECT pa.reference,pa.supplier_reference,al.name
          		FROM ' . _DB_PREFIX_ . 'product_attribute pa JOIN ' . _DB_PREFIX_ . 'product_attribute_combination pac ON pa.id_product_attribute = pac.id_product_attribute
          		JOIN ' . _DB_PREFIX_ . 'attribute_lang al ON pac.id_attribute = al.id_attribute
          		WHERE pa.id_product = ' . $prod ['id_product'] . ' AND al.id_lang = ' . $id_lang . ';' );
					$cnt_attrs = count ( $attrs );
					
					$keywords = $this->mod->setMetaKeywords ( preg_replace ( '#(' . preg_quote ( $prod ['supplier_reference'], '#' ) . '|' . $wordsfilter . ')+#uims', '', $this->mod->removeShortWords ( $prod ['name'], true ) ) );
					if (! empty ( $this->mod->brands ) || ! empty ( $this->mod->families ) || ! empty ( $this->mod->meta_title_pattern )) {
						preg_match_all ( '#(' . $this->mod->brands . ')+#uims', $keywords, $brand );
						$brand = implode ( ' ', $brand [1] );
						preg_match_all ( '#(' . $this->mod->families . ')+#uims', $keywords, $family );
						$family = implode ( ' ', $family [1] );
						$model = preg_replace ( '#(' . $this->mod->brands . '|' . $this->mod->families . ')+#uims', '', $keywords );
												
						$meta_composed = preg_replace ( '#(%brand%)#umis', trim ( $brand, ' ' ), $this->mod->composedtags );
						$meta_composed = preg_replace ( '#(%family%)#umis', trim ( $family, ' ' ), $meta_composed );
						$meta_composed = preg_replace ( '#(%model%)#umis', trim ( $model, ' ' ), $meta_composed );
						
						$meta_title_composed = preg_replace ( '#(%brand%)#umis', trim ( $brand, ' ' ), $this->mod->meta_title_pattern );
						$meta_title_composed = preg_replace ( '#(%family%)#umis', trim ( $family, ' ' ), $meta_title_composed );
						$meta_title_composed = preg_replace ( '#(%model%)#umis', trim ( $model, ' ' ), $meta_title_composed );
						$prod ['meta_title'] = $meta_title_composed;
					}
					
					if ($useref == 'on') {
						if ($cnt_attrs > 0) {
							foreach ( $attrs as $attr )
								if (! empty ( $attr ['reference'] ) && (mb_stripos ( $supkws, $attr ['reference'] ) === false))
									$prod ['reference'] .= ',' . $attr ['reference'];
						}
						$supkws .= ',' . $prod ['reference'];
					}
					
					if ($usesupref == 'on') {
						if ($cnt_attrs > 0) {
							foreach ( $attrs as $attr )
								if (! empty ( $attr ['supplier_reference'] ) && (mb_stripos ( $supkws, $attr ['reference'] ) === false))
									$prod ['supplier_reference'] .= ',' . $attr ['supplier_reference'];
						}
						$trimmedref = preg_replace ( '#[^a-z0-9,]+#uims', '', $prod ['supplier_reference'] );
						$supkws .= ',' . $prod ['supplier_reference'] . ($prod ['supplier_reference'] != $trimmedref ? ',' . $trimmedref : '');
					}
					
					if ($usetitle == 'on') {
						$trimtitle = explode ( ' ', $this->mod->removeShortWords ( $prod ['name'], true ) );
						foreach ( $trimtitle as $tt )
							if (mb_stripos ( $tt, $supkws ) === false)
								$supkws .= ',' . trim($tt);
					}
					
					if ($cnt_attrs > 0) {
						foreach ( $attrs as $attr )
							if (! empty ( $attr ['name'] )) {
								$kws = explode ( ' ', $attr ['name'] );
								foreach ( $kws as $kw )
									if (mb_stripos ( $prod ['attrs'], $kw ) === false) {
										$prod ['attrs'] .= trim($kw) . ',';
									}
							}
					}
					
					$supkws = mb_strtolower ( $this->mod->setMetaKeywords ( $supkws . ',' . $meta_composed ) );
					
					// get tags
					$tags = TagCore::getProductTags ( $prod ['id_product'] );
					$tmptags = array ();
					foreach ( $tags [$this->mod->id_lang] as $tag )
						if (! in_array ( $tag, $tmptags ))
							$tmptags [] = trim($tag);
					$prod ['tags'] = implode ( ',', $tmptags ) . ',';
					
					if ($add2mk == 'in') {
						$meta_supkws = $this->sanitizeKeywords ( $supkws, $prod ['meta_keywords'] );
						$meta_supkws = $this->mod->replaceChars ( $meta_supkws, $this->mod->equivalents );
						if (strlen ( $meta_supkws ) > 0) {
							if ($add2pos == 'prepend')
								$prod ['meta_keywords'] = trim ( $meta_supkws . ',' ) . trim ($prod ['meta_keywords']);
							else
								$prod ['meta_keywords'] .= rtrim ( $meta_supkws . ',' );
						}
					}
					
					if ($add2tags == 'in') {
						$tag_supkws = $this->sanitizeKeywords ( $supkws, $prod ['tags'] );
						$tag_supkws = $this->mod->replaceChars ( $tag_supkws, $this->mod->equivalents );
						if (strlen ( $tag_supkws ) > 0) {
							if ($add2pos == 'prepend')
								$prod ['tags'] = trim ( $tag_supkws . ',' ) . trim($prod ['tags']);
							else
								$prod ['tags'] .= rtrim ( $tag_supkws, ',' ) . ',';
						}
					}
					
					$this->refs [$prod ['id_product']] = array (
							'name' => $prod ['name'],
							'meta_keywords' => $prod ['meta_keywords'],
							'meta_title' => $prod ['meta_title'],
							'meta_description' => $prod ['meta_description'],
							'link_rewrite' => $prod ['link_rewrite'],
							'tags' => $prod ['tags'],
							'unmodified' => $prod['unmodified'] 
					);
				}
			} else if (Tools::isSubmit ( 'submitTags' )) {
				
				if (empty ( $this->ref )) {
					$this->mod->addError ( $this->l ( 'No reference supplied!' ) );
					return false;
				}
				
				$this->refs = Tools::getValue ( 'prods', '' );
				$prods = $this->refs;
				
				if (empty ( $prods )) {
					$this->mod->addError ( $this->l ( 'Parameters missing for update!' ) );
				} else {
					$i = 0;
					$up = 0;
					foreach ( $prods as $id_prod => $prod ) {
						if ((intval ( $id_prod ) > 0) && ! empty ( $prod ['link_rewrite'] ) && ! empty ( $prod ['meta_keywords'] ) && ! empty ( $prod ['meta_title'] ) && ! empty ( $prod ['meta_description'] )) {
							
							// Update metas
							$sql = "UPDATE " . _DB_PREFIX_ . "product_lang SET meta_title = '" . pSQL ( $prod ['meta_title'] ) . "', meta_keywords = '" . pSQL ( $prod ['meta_keywords'] ) . "', meta_description = '" . pSQL ( $prod ['meta_description'] ) . "', link_rewrite = '" . pSQL ( preg_replace('#[^a-z0-9\-]+#i','',$prod ['link_rewrite']) ) . "' WHERE id_product = '{$id_prod}' AND id_lang = '{$this->mod->id_lang}'";
							$r = $this->db->execute ( $sql );
							
							// Recreate tags assoc
							if ($r !== false) {
								Tag::deleteTagsForProduct ( $id_prod );
								if (Tag::addTags ( 2, $id_prod, $this->mod->setMetaKeywords ( $prod ['tags'] ) ))
									$up ++;
							} else
								$this->mod->addError ( $this->l ( 'Database engine error' ) . ': ' . $this->db->getMsgError () );
						} else
							$this->mod->addError ( sprintf ( $this->l ( 'Tags & meta are not processed for product ID "%s", a parameter at least was empty.' ), $id_prod ) );
						$i ++;
					}
				}
				echo sprintf ( $this->l ( '%s Products found & %s updated' ), $i, $up );
			}
		} else if ($this->tab_id == 2) {
			/* Images post Process */
			if (Tools::isSubmit ( 'displayImages' ) && (Tools::getValue ( 'displayImages' ) == $this->l ( 'Display' ))) {
				$this->ref = Tools::getValue ( 'ref', '' );
				
				$sql = "SELECT id_product FROM " . _DB_PREFIX_ . "product WHERE " . $this->mod->dbfield . " LIKE '" . pSQL ( $this->ref ) . "' ORDER BY id_product DESC LIMIT 1;";
				$pd = $this->db->executeS ( $sql );
				if (isset ( $pd [0] )) {
					$productImages = Image::getImages ( $id_lang, $pd [0] ['id_product'] );
					if (count ( $productImages ) > 0) {
						foreach ( $productImages as $ii => $image ) {
							$path = ImageCore::getImgFolderStatic ( $image ['id_image'] );
							$this->img [] = _PS_IMG_ . 'p/' . $path . $image ['id_image'] . '-medium.jpg';
						}
					}
				}
			} else if (Tools::isSubmit ( 'submitImages' )) {
				// Replace images for products
				$this->ref = Tools::getValue ( 'ref', '' );
				$imgs [0] = Tools::getValue ( 'cover_url', '' );
				$imgs = array_merge ( $imgs, Tools::getValue ( 'img_url', '' ) );
				
				$sql = "SELECT p.id_product FROM " . _DB_PREFIX_ . "product p JOIN " . _DB_PREFIX_ . "product_attribute pa ON p.id_product = pa.id_product WHERE pa." . $this->mod->dbfield . " LIKE '" . pSQL ( $this->ref ) . "' GROUP BY pa.id_product ORDER BY p.id_product DESC, pa.id_product_attribute ASC;";
				$rids = $this->mod->db->ExecuteS ( $sql );
				$pids = $rids [0] ['id_product'];
				for($i = 1; $i < count ( $rids ); $i ++)
					$pids .= ',' . $rids [$i] ['id_product'];
				
				switch ($this->mod->dbfield) {
					case 'reference' :
						$sql = "SELECT p.id_product, IF(pa.id_product_attribute IS NULL,0,pa.id_product_attribute) AS id_product_attribute FROM " . _DB_PREFIX_ . "product p LEFT JOIN " . _DB_PREFIX_ . "product_attribute pa ON p.id_product = pa.id_product WHERE p." . $this->mod->dbfield . " LIKE '" . pSQL ( $this->ref ) . "' OR pa." . $this->mod->dbfield . " LIKE '" . pSQL ( $this->ref ) . "' AND p.id_product NOT IN ({$pids}) ORDER BY p.id_product DESC, pa.id_product_attribute ASC;";
						break;
					case 'supplier_reference' :
						if (IS_PS_15)
							$sql = "SELECT p.id_product FROM " . _DB_PREFIX_ . "product p LEFT JOIN " . _DB_PREFIX_ . "product_supplier ps ON p.id_product = ps.id_product WHERE ps.product_" . $this->mod->dbfield . " LIKE '" . pSQL ( $this->ref ) . "' ORDER BY p.id_product DESC;";
						else
							$sql = "SELECT p.id_product, IF(pa.id_product_attribute IS NULL,0,pa.id_product_attribute) AS id_product_attribute FROM " . _DB_PREFIX_ . "product p LEFT JOIN " . _DB_PREFIX_ . "product_attribute pa ON p.id_product = pa.id_product WHERE p." . $this->mod->dbfield . " LIKE '" . pSQL ( $this->ref ) . "' OR pa." . $this->mod->dbfield . " LIKE '" . pSQL ( $this->ref ) . "' ORDER BY p.id_product DESC, pa.id_product_attribute ASC;";
						break;
					default :
						$this->mod->addError ( $this->l ( 'Invalid configuration parameters' ) );
				}
				$pd = $this->db->executeS ( $sql );
				
				if (isset ( $pd [0] )) {
					foreach ( $pd as $p ) {
						if (intval ( $p ['id_product_attribute'] ) > 0) {
							$sql = 'SELECT id_image FROM ' . _DB_PREFIX_ . 'product_attribute_image WHERE id_product_attribute = ' . $p ['id_product_attribute'] . ';';
						} else {
							$sql = 'SELECT id_image FROM ' . _DB_PREFIX_ . 'image WHERE id_product = ' . $p ['id_product'] . ';';
						}
						$productImages = $this->db->executeS ( $sql );
						for($i = 0; $i < count ( $productImages ); $i ++) {
							$image = $productImages [$i];
							$this->mod->deleteImg ( $image, $p ['id_product'], $p ['id_product_attribute'] );
						}
						
						for($i = 0; $i < count ( $imgs ); $i ++) {
							if ($imgs [$i] != '') {
								$this->mod->addImage ( $imgs [$i], $p ['id_product'], $i, $p ['id_product_attribute'] );
							}
						}
					}
				}
			}
		} else if ($this->tab_id == 3) {
			if (Tools::isSubmit ( 'displayDescs' )) {
				$this->ref = Tools::getValue ( 'ref', '' );

				$sql = "SELECT p.id_product, pl.name, pl.description_short, pl.description FROM " . _DB_PREFIX_ . "product AS p JOIN " . _DB_PREFIX_ . "product_lang AS pl ON p.id_product = pl.id_product LEFT JOIN " . _DB_PREFIX_ . "product_attribute pa ON p.id_product = pa.id_product WHERE pl.id_lang = {$this->mod->id_lang} AND (p.{$this->mod->dbfield} LIKE '{$this->ref}' OR pa.{$this->mod->dbfield} LIKE '{$this->ref}');";
				$prods = $this->db->executeS ( $sql );
				$this->refs = array ();
				foreach ( $prods as $prod ) {
					
					$this->refs [$prod ['id_product']] = array (
							'name' => $prod ['name'],
							'description_short' => $prod ['description_short'],
							'description' => $prod ['description'] 
					);
				}
			} else if (Tools::isSubmit ( 'submitDescs' )) {
				$description_short = Tools::getValue ( 'description_short' );
				$description = Tools::getValue ( 'description' );
				$listprods = Tools::getValue ( 'prods' );
				$parms1 = Tools::getValue ( 'parms1' );
				$parms2 = Tools::getValue ( 'parms2' );
				$parms3 = Tools::getValue ( 'parms3' );
				$parms4 = Tools::getValue ( 'parms4' );
				
				foreach($listprods as $id_product => $checked) {
					if((intval($id_product) > 0 ) && ($checked==$id_product))						
				   $ids_products[] = $id_product;
				}

				$result = '';
				
				$sql = "SELECT p.id_product, p.reference, p.supplier_reference, pl.name FROM " . _DB_PREFIX_ . "product AS p JOIN " . _DB_PREFIX_ . "product_lang AS pl ON p.id_product = pl.id_product LEFT JOIN " . _DB_PREFIX_ . "product_attribute pa ON p.id_product = pa.id_product WHERE pl.id_lang = {$this->mod->id_lang} AND (p.id_product IN (".implode(',',$ids_products)."));";
				$prods = $this->db->executeS ( $sql );
				$this->refs = array ();
				foreach ( $prods as $prod ) {
					$prod ['attrs'] = '';
						
					$attrs = $this->db->executeS ( 'SELECT pa.reference,pa.supplier_reference,al.name
          		FROM ' . _DB_PREFIX_ . 'product_attribute pa JOIN ' . _DB_PREFIX_ . 'product_attribute_combination pac ON pa.id_product_attribute = pac.id_product_attribute
          		JOIN ' . _DB_PREFIX_ . 'attribute_lang al ON pac.id_attribute = al.id_attribute
          		WHERE pa.id_product = ' . $prod ['id_product'] . ' AND al.id_lang = ' . $id_lang . ';');
					$cnt_attrs = count ( $attrs );
				
					$keywords = $this->mod->setMetaKeywords ( preg_replace ( '#(' . preg_quote ( $prod ['supplier_reference'], '#' ) . '|' . $wordsfilter . ')+#uims', '', $this->mod->removeShortWords ( $prod ['name'], true ) ) );
					if (! empty ( $this->mod->brands ) || ! empty ( $this->mod->families )) {
						preg_match_all ( '#(' . $this->mod->brands . ')+#uims', $keywords, $brand );
						$brand = implode ( ' ', $brand [1] );
						preg_match_all ( '#(' . $this->mod->families . ')+#uims', $keywords, $family );
						$family = implode ( ' ', $family [1] );
						$model = preg_replace ( '#(' . $this->mod->brands . '|' . $this->mod->families . ')+#uims', '', $keywords );
				
						$desc_short = preg_replace ( '#(%brand%)#umis', trim ( $brand, ' ' ), $description_short );
						$desc_short = preg_replace ( '#(%family%)#umis', trim ( $family, ' ' ), $desc_short );
						$desc_short = preg_replace ( '#(%model%)#umis', trim ( $model, ' ' ), $desc_short );
				
						$desc = preg_replace ( '#(%brand%)#umis', trim ( $brand, ' ' ), $description );
						$desc = preg_replace ( '#(%family%)#umis', trim ( $family, ' ' ), $desc );
						$desc = preg_replace ( '#(%model%)#umis', trim ( $model, ' ' ), $desc );
						
						/* A vérifier */
						for($i = 1; $i <= 4; $i++) {
							$parm = ${"parms{$i}"};
							$cntd = 0;
							$cntds = 0;
							if(!empty($parm[$prod['id_product']])) {					  	
						  	$desc_short = str_ireplace("%parm{$i}%", $parm[$prod['id_product']], $desc_short, $cntd );
						  	$desc = str_ireplace ("%parm{$i}%", $parm[$prod['id_product']], $desc, $cntds );						
						  } 
						  if($cntds == 0) 
						  	$desc_short = str_ireplace ( "%parm{$i}%", '', $desc_short );
						  if($descd == 0)
						  	$desc = str_ireplace ("%parm{$i}%", '', $desc );
						}
					}
					
					$sql = 'UPDATE ' . _DB_PREFIX_ . 'product_lang SET description_short = "' . pSQL ( $desc_short, true ) . '", `description` = "' . pSQL ( $desc, true ) . '" WHERE id_product = ' . intval ( $prod ['id_product'] ) . ' AND id_lang = ' . $id_lang . ' LIMIT 1;';
					$this->db->Execute ( $sql );
					$result .= $this->l ( 'Descriptions updated for ' ) . $prod['id_product'].'<br/>';
						
				}				
				
				echo '<div class="conf confirm">'.$result.'</div>';
			}
		} else if($this->tab_id == 4) {
			
			$this->ref = Tools::getValue ( 'ref', '' );
			$submitUpdate = Tools::isSubmit('submitLinks');				
			if(Tools::isSubmit('submitLinks')) {
				$this->ref = $_POST['ref'];
				$id_links = $_POST['id_link'];
				$id_product_src = $_POST['id_product_src'];
				$id_product_dst = $_POST['id_product_dst'];
				$values = $_POST['value'];
				$sqltpl= 'INSERT INTO '._DB_PREFIX_.'crosslinks (id_product_src,id_product_dst,value) VALUES(%d,%d,\'%s\');';
				$prodids=array();
				foreach($id_links as $id_link) {
					if(!in_array(intval($id_product_src[$id_link]),$prodids))
					  $prodids[] = intval($id_product_src[$id_link]);
				}
				$this->db->Execute('DELETE FROM '._DB_PREFIX_.'crosslinks WHERE id_product_src IN ('.implode(',',$prodids).');');
				foreach($id_links as $id_link) {
					$sql = sprintf($sqltpl,$id_product_src[$id_link],$id_product_dst[$id_link],htmlentities($values[$id_link],ENT_QUOTES));
					$this->db->Execute($sql);						
				}
			}			
			if(Tools::isSubmit('submitSearch') || $submitUpdate) {
				$sql = 'SELECT p.id_product, pl.name FROM '
					._DB_PREFIX_.'product p JOIN '. _DB_PREFIX_ . "product_lang pl ON p.id_product = pl.id_product AND pl.id_lang = {$id_lang} "
				  ."WHERE (p.{$this->mod->dbfield} LIKE '{$this->ref}');";
				$prods = $this->db->executeS ( $sql );
				$this->refs = array ();
				foreach($prods as $ref) {
					$sql = 'SELECT * FROM ' . _DB_PREFIX_ . "crosslinks WHERE id_product_src = {$ref['id_product']};";
					$clinks = $this->db->executeS($sql);
					$crosslinks = array();
					foreach($clinks as $clink) {
						$crosslinks[$clink['id']] = array(
								'id_product_src' => $clink['id_product_src'],
								'id_product_dst' => $clink['id_product_dst'],
								'value' => $clink['value']
						);
					}
				  $this->refs [$ref ['id_product']] = array (
							'name' => $ref['name'],
							'crosslinks' => $crosslinks
					);
				}
				
			}			
		}
	}
}
?>