<?php
if (version_compare(_PS_VERSION_, '1.5', '<'))
	include_once(_PS_TOOL_DIR_.'/backward_compatibility/backward.php');

define('IS_PS_15',version_compare(_PS_VERSION_, '1.5', '<')?false:true);

class refman extends Module {
  public $name = 'refman';
  public $need_instance = 1;
  public $tabClass = 'AdminRefMan';
  public $tab ='others';
  public $usetitle, $useref, $usesupref, $excwords, $brands, $families, $composedtags, $numfields, $meta_title_pattern;
  public $token, $id_lang, $context;
  public $is15 = IS_PS_15;
  public $controller = 'tab';
  protected $_html = '';
  private $imgsCnt = 0;
  
  private static $logfilename = 'refman.log';
  
  private $errors = array();
  
  public $equivalents = array(
      0 => array('[\t\s\r\n]{2,}',' '),
      1 => array('[!<>;?=+#°{}_$]+', ''),
      2 => array('(,[ .-]*,)', ',')
  );
  
  public function __construct() {
    parent::__construct();
    $this->displayName = $this->l('Manage some products aspects by reference');
    $this->description = $this->l('See later ...');
    $this->confirmUninstall = $this->l('Are you sure you want to uninstall this module ?');
    
    
    $this->version = '0.2';
    $this->author = 'Matsky';
    $this->error = false;
    $this->valid = false;
    $this->db = Db::getInstance();
    $this->tab = 'others';
    $this->need_instance = 1;

    /** Backward compatibility */
    $this->context = Context::getContext();
    $this->id_lang = $this->context->language->id;
    $this->token = Tools :: getValue('token');
    
    $this->dbfield = Configuration::get("{$this->name}_dbfield");
    $this->excwords = Configuration::get("{$this->name}_excludewords");
    $this->brands = Configuration::get("{$this->name}_brands");
    $this->families = Configuration::get("{$this->name}_families");
    $this->composedtags = Configuration::get("{$this->name}_composedtags");
    $this->meta_title_pattern = Configuration::get("{$this->name}_meta_title_pattern");
    $this->numfields = Configuration::get("{$this->name}_numfields");
    
    $this->controller = $this->is15?'controller':'tab';
  }

  public function install() {
    Db::getInstance()->execute('CREATE TABLE '._DB_PREFIX_."crosslinks (id integer primary key auto_increment,id_product_src integer not null, id_product_dst integer not null, value varchar(32) not null);");
    Db::getInstance()->execute('ALTER TABLE `'._DB_PREFIX_.'crosslinks` ADD UNIQUE( `id_product_src`, `id_product_dst`, `value`);');
    Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_."configuration (name,value) VALUES('{$this->name}_dbfield','reference'),('{$this->name}_usetitle','on'),('{$this->name}_useref','on');");
    @copy(_PS_MODULE_DIR_ . $this->name . '/logo.gif', _PS_IMG_DIR_ . 't/' . $this->name . '.gif');
    @copy(_PS_MODULE_DIR_ . $this->name . '/logo.gif', _PS_ADMIN_IMG_ . 't/' . $this->tabClass . '.gif');

    if( (parent::install() == false)
    	|| ($this->registerHook('header') == false)
    	|| ($this->installTab() == false))  return false;
    return true;
  }

  public function uninstall() {
    Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_."configuration WHERE name LIKE '{$this->name}_%';");
    @unlink(_PS_IMG_DIR_ . 't/' . $this->name . '.gif');
    if ($this->uninstallTab() == false OR parent::uninstall() == false) {
      return false;
    }
    return true;
  }
  
  public function hookHeader($params) {
  	global $smarty;
  	$callback = array(&$this,'crossLinksSmarty');
  	$type =	smartyRegisterFunction($smarty, 'function', 'crossLinks', $callback);  	
  }
  
  public function crossLinksSmarty($params) {  	
    $id_product_src = intval($params['id_product']);
    
    $joinTbls = _DB_PREFIX_."crosslinks cl INNER JOIN "._DB_PREFIX_."product p
    						ON (cl.id_product_dst = p.id_product)
    						INNER JOIN "._DB_PREFIX_."product_lang pl 
    						ON (cl.id_product_dst = pl.id_product AND pl.id_lang =  {$this->context->language->id})
    						INNER JOIN `"._DB_PREFIX_."category_lang` clng
    						ON (p.`id_category_default` = clng.`id_category` AND clng.`id_lang` = {$this->context->language->id})";
    
    
    $linked = Db::getInstance()->executeS("SELECT cl.*, pl.link_rewrite as prewrite, clng.link_rewrite as crewrite FROM  {$joinTbls} WHERE cl.id_product_src = {$id_product_src} ORDER BY cl.`value` ASC;");
    $params['product']->translation_attached = array('Compatible version' => $this->l('Compatible version'),'Compatibles versions' => $this->l('Compatibles versions'));
    $params['product']->clinks = $linked;
  }
  
  private function installTab() {
    @copy(_PS_MODULE_DIR_ . $this->name . '/logo.gif', _PS_IMG_DIR_ . 't/' . $this->tabClass . '.gif');
    $tab = new Tab();
    $tab->class_name = $this->tabClass;
    $tab->module = $this->name;
    foreach (Language::getLanguages() as $language) {
      $tab->name[$language['id_lang']] = $this->l('Manage by Reference');
    }
		$tab->id_parent = Tab::getIdFromClassName('AdminTools');
        
		$hook_name = $this->is15 ?'displayBackOfficeHeader':'backOfficeHeader';
    if (!$tab->save() || !$this->registerHook($hook_name)) {
      return false;
    }
    return true;
  }
  
  private function uninstallTab() {
    $idTab = Tab::getIdFromClassName($this->tabClass);
    if ($idTab != 0) {
      @unlink(_PS_IMG_DIR_ . 't/' . $this->tabClass . '.gif');
      $tab = new Tab($idTab);
      $tab->delete();
      return true;
    }
    return false;
  }
  
  /**
   *
   * @param unknown $params
   * @return string
   */
  public function hookBackOfficeHeader($params) {
  	$html = '';
  	if ((Tools :: getValue('configure') == 'refman') || (strtolower(Tools :: getValue($this->controller)) == strtolower('AdminRefMan'))) {
  
  		$iso = Language :: getIsoById((int) ($this->context->cookie->id_lang));
  		$isoTinyMCE = (file_exists(_PS_ROOT_DIR_ . '/js/tiny_mce/langs/' . $iso . '.js') ? $iso : 'en');
  		$ad = dirname($_SERVER["PHP_SELF"]);
  
  		$html = '<link rel="stylesheet" type="text/css" href="' . $this->_path . 'rmAdminTab.css"/>' . "\r\n";
  
  		$html .= '<script type="text/javascript">' . "\r\n" .
  				'var iso = \'' . $isoTinyMCE . '\' ;' . "\r\n" .
  				'var pathCSS = \'' . _THEME_CSS_DIR_ . '\' ;' . "\r\n" .
  				'var ad = \'' . $ad . '\' ;' . "\r\n" .
  				'</script>' . "\r\n" .
  				'<script type="text/javascript" src="' . __PS_BASE_URI__ . 'js/tiny_mce/tiny_mce.js"></script>'. "\r\n"
  				.'<script type="text/javascript" src="' . $this->_path . 'refman-office.js.php"></script>' . "\r\n";
  	}
  	return $html;
  }
  
  
  /**
   *
   * @param String $str
   */
  public function addError($str,$isHtml=false) {
    if($isHtml===false)
      $this->errors[] = htmlspecialchars($str,ENT_NOQUOTES,'UTF-8',false);
    else $this->errors[] = $str;
  }
  
 /**
  * Log errors to file and flush error buffer
  *
  * @param boolean $return force to return a string
  */
  public function printErrors($return = false) {
    $retstr = null;
    foreach($this->errors as $idx => $error) {
      if($return==true)
        $retstr .= $error."\r\n";
      else
        file_put_contents('/var/log/'.self::$logfilename,$error."\r\n",FILE_APPEND);
    }
    $this->errors = array();
    if($return===true)
      return $retstr;
  }
  
  public function hasErrors() {
    return count($this->errors)>0;
  }
  
  
  /**
   * @param unknown $field
   * @return multitype:unknown
   */
  public function createMultiLangField($field) {
  	$languages = Language :: getLanguages(false);
  	$res = array ();
  	foreach ($languages AS $lang)
  		$res[$lang['id_lang']] = $field;
  	return $res;
  }
  
  /**
   * @param int $id_entity
   * @param Image $image
   * @param string $url
   * @param string $entity
   * @return boolean
   */
  public function copyImg($id_entity, $image = NULL, $url, $entity = 'products') {
  	try {
  		$id_image = $image->id;
  		$tmpfile = tempnam(_PS_TMP_IMG_DIR_, 'ps_import');
  		//			$watermark_types = explode(',', Configuration::get('WATERMARK_TYPES'));
  
  		switch ($entity) {
  			default :
  			case 'products' :
  				$path = $image->getPathForCreation();
  				if (!$path)
  					throw new Exception('Cannot create folder "' . $path . '" for image(s).');
  				break;
  			case 'categories' :
  				$path = _PS_CAT_IMG_DIR_ . (int) ($id_entity);
  				break;
  		}
  
  		if (copy($url, $tmpfile)) {
  			$finfo = finfo_open(FILEINFO_MIME_TYPE);
  			$mt = finfo_file($finfo, $tmpfile);
  			finfo_close($finfo);
  
  			switch ($mt) {
  				case 'image/jpeg' :
  					$if = 'jpg';
  					break;
  				case 'image/png' :
  					$if = 'png';
  					break;
  				case 'image/gif' :
  					$if = 'gif';
  					break;
  				default :
  					$if = '';
  			}
  			if (empty ($if))
  				throw new Exception('Cannot determine mime for image "' . $url . '".');
  			/*
  			 if($if != $image->image_format)
  				$image->image_format = $if;
  			*/
  			$imagesTypes = ImageType :: getImagesTypes($entity);
  
  			if (substr(_PS_VERSION_, 0, 3) < '1.5') {
  				$r = imageResize($tmpfile, $path . '.' . $image->image_format, null, null, $image->image_format);
  				if ($r === false)
  					throw new Exception($this->l('ImageResize failed!'));
  			}
  			foreach ($imagesTypes AS $k => $imageType) {
  
  				if (substr(_PS_VERSION_, 0, 3) > '1.4') {
  					$r = ImageManager :: resize($tmpfile, $path . '-' . $imageType['name'] . '.' . $image->image_format, $imageType['width'], $imageType['height'], $image->image_format);
  				} else {
  					$r = imageResize($tmpfile, $path . '-' . $imageType['name'] . '.' . $image->image_format, $imageType['width'], $imageType['height'], $image->image_format);
  					if ($r === false)
  						throw new Exception($this->l('ImageResize failed!'));
  				}
  			}
  			/*
  			 if (in_array($imageType['id_image_type'], $watermark_types))
  				Module::hookExec('watermark', array('id_image' => $id_image, 'id_product' => $id_entity));
  			*/
  		} else {
  			throw new Exception('Cannot copy file from "' . $url . '" to "' . $tmpfile . '".');
  		}
  
  		unlink($tmpfile);
  		return true;
  	} catch (Exception $e) {
  		$this->addError($e->getMessage());
  		@ unlink($tmpfile);
  		return false;
  	}
  }

  /**
   *
   * @param string $url Url of the image
   * @param integer $id_prod Product ID
   * @param integer $position Position of the image
   * 
   * @return void Void
   */
  public function addImage($url, $id_prod = 0, $position = 0, $combinationId = false) {
  	if (strlen($url) == 0)
  		return;
  	
  	set_time_limit(10);
  	
  	$productHasImages = (bool) ImageCore::getImages($this->context->language->id, (int) ($id_prod));
  	
 		$ex = $this->file_exists($url);
 		if ($ex === false) {
 			$this->addError(sprintf($this->l('Image "%s" file not exist.'), $url));
 			return;
 		} else if ($ex === 'error') {
 			$this->addError(sprintf($this->l('Image "%s" produce an error.'), $url));
 			return;
 		}
 		
  	$prod = new Product($id_prod);
  	$img = new ImageCore(null,$this->context->language->id);
  	$img->id_product = $id_prod;  	
  	$img->position = $img->getHighestPosition($id_prod)+1;
  	$img->cover = ($position==0) ? true : false;
  	$img->legend = '/!\\'.$prod->name[$this->context->language->id];
  	if (($fieldError = $img->validateFields(UNFRIENDLY_ERROR, true)) === true AND ($langFieldError = $img->validateFieldsLang(UNFRIENDLY_ERROR, true)) === true AND $img->add(true,false)) {
  		$resadd = $this->copyImg($id_prod, $img, $url);
  		if ($resadd == true) {
  			if($combinationId) {
  				$sql = "INSERT INTO "._DB_PREFIX_."product_attribute_image (id_product_attribute,id_image) VALUE('{$combinationId}','{$img->id}');";
  				$this->db->execute($sql);  				
  			}
  			$this->imgsCnt++;
  		}
  	} else {
  		$errDbMsg = DbCore::getInstance()->getMsgError();
  		$this->addError($img->legend[$this->context->language->id] . (isset ($img->id_product) ? ' (' . $img->id_product . ')' : '') . ' ' . Tools :: displayError('cannot be saved') . ' ' .
  			 ($fieldError !== true ? $fieldError : '') . ($langFieldError !== true ? $langFieldError : '').(!empty($errDbMsg)?'DB Error:'.$errDbMsg:''));
  	}
  }
    
  public function file_exists($filename) {
  	if (stripos($filename, 'http://') !== false) {
  		try {
  			$ch = curl_init();
  			curl_setopt($ch, CURLOPT_URL, $filename);
  			curl_setopt($ch, CURLOPT_HEADER, true);
  			curl_setopt($ch, CURLOPT_NOBODY, true);
  			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'HEAD');
  			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  
  			$r = curl_exec($ch);
  			$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  			curl_close($ch);
  
  			if ($code == 200 || $code == 302)
  				return true;
  			else
  				return false;
  
  		} catch (Exception $e) {
  			$this->addError($e->getMessage());
  			return 'error';
  		}
  		return false;
  	} else
  		return file_exists($filename);
  }
  
  
  /**
   * 
   * 
   * @param Image $image
   * @param string $entity
   * @return boolean
   */
  public function deleteImg($image = NULL, $id_product = 0, $combinationId = false) {
  	try {
  		$entity = 'products';
  		$id_image = $image['id_image'];
  
			$imagesTypes = ImageType::getImagesTypes($entity);

 			foreach ($imagesTypes AS $k => $imageType) {
 				$path = _PS_IMG_DIR_.'p/'.preg_replace('#([0-9]+?)#is','$1/',$image['id_image']).$image['id_image'];
 				if(file_exists($path.'-' . $imageType['name'] . '.jpg'))
				  $r = @unlink($path . '-' . $imageType['name'] . '.jpg');				  
 				else $this->addError(sprintf('Nothing to delete (%s)',$path.'-' . $imageType['name'] . '.jpg'));
 			}

 			$c=0;
 			$r = true;
 			
 			if($combinationId) {
 				$r = $this->db->execute("DELETE FROM "._DB_PREFIX_."product_attribute_image WHERE id_image = '{$image['id_image']}' AND id_product_attribute = '{$combinationId}' LIMIT 1;");
 				if ($r === false)
 					$this->addError($this->l('Image attribute deletion failed!'));
 					
 				$c = $this->db->getRow("SELECT COUNT(*) AS Cnt FROM "._DB_PREFIX_."product_attribute_image WHERE id_image = '{$image['id_image']}'");
 				if ($c === false)
 					$this->addError($this->l('Image count failed!'));
 				$c = intval($c['Cnt']);
 			}
 			if($c == 0) {
 			  $sql = "DELETE FROM "._DB_PREFIX_."image_lang WHERE id_image = '{$image['id_image']}' LIMIT 1;";
 			  $this->db->execute($sql);
 			  if ($r === false)
 				  $this->addError($this->l('Image deletion failed!'));
 			  $sql = "DELETE FROM "._DB_PREFIX_."image WHERE id_product = '{$id_product}' AND id_image = '{$image['id_image']}';";
 			  $r = $this->db->execute($sql);
 			  if ($r === false)
 				  $this->addError($this->l('Image table deletion failed!'));
 			} 
  		return $r;
  	} catch (Exception $e) {
  		$this->addError($e->getMessage());
  		@unlink($tmpfile);
  		return false;
  	}
  }
  
  
  public function replaceChars($str,$equiv) {
    $result = $str;
    foreach($equiv AS $elemstr) {
      $pattern = str_replace('#', '\#', $elemstr[0]);
      $result = preg_replace('#(*UTF8)'.$pattern.'#is', $elemstr[1], $result);
    }
    return $result;
  }
  
  public function setMetaKeywords($str) {
    return $this->replaceChars($str,$this->equivalents);
  }
  
  public function removeShortWords($src_str, $return_str = false) {
    $kws_cnt = preg_match_all('#([a-z0-9éàèêùôîâûç\-.%]{3,}| HP )#uims',$src_str,$matches);
    if($kws_cnt > 0) {
      $out = $return_str?'':array();
      for($i = 0; $i < $kws_cnt; $i++) {
        $matches[0][$i] = preg_replace('#([^a-z0-9éàèêùôîâûç\-.%])+#uims','',$matches[0][$i]);
        if($return_str)
          $out .= $matches[0][$i].(($i < $kws_cnt-1)?' ':'');
        else $out[] = $matches[0][$i];
      }
      return $out;
    }
    return false;
  }

  public function getContent() {
    if(count($_POST)>0)
      $this->_postProcess();
    $this->_displayForm();
    return $this->_html;
  }
  
  private function _displayForm() {
    $dbfield = Configuration::get("{$this->name}_dbfield");
    $excwords = Configuration::get("{$this->name}_excludewords");
    $brands = Configuration::get("{$this->name}_brands");
    $families = Configuration::get("{$this->name}_families");
    $comptags = Configuration::get("{$this->name}_composedtags");
    $numfields = Configuration::get("{$this->name}_numfields");
    $meta_title_pattern = Configuration::get("{$this->name}_meta_title_pattern");
    
    $this->_html .= '<center>'
    .'<form action="?'.$this->controller.'=AdminModules&configure='.$this->name.'&tab_module=others&module_name='.$this->name.'&token='.$this->token.'" method="post">'
    .'<fieldset class="width4"><legend><img src="'._PS_IMG_.'t/'.$this->name.'.gif" />'.$this->l('Tags configuration').'</legend>'
    .'<label>'.$this->l('Choose reference field scanned :').' </label>'
    .'<div class="margin-form">'
    .'<select name="dbfield"><option value="reference"'.($dbfield=='reference'?' selected ':'').'>'.$this->l('Reference').'</option>
      <option value="supplier_reference"'.($dbfield=='supplier_reference'?' selected ':'').'>'.$this->l('Supplier reference').'</option></select>'
    .'</div><br/>'
    .'<label>'.$this->l('List of words to exclude to determine models :').' </label>'
    .'<div class="margin-form">'
    .'<textarea name="excwords" cols="48" rows="4">'.$excwords.'</textarea>'
    .'</div><br/>'
    .'<label>'.$this->l('List brands to determine models :').' </label>'
    .'<div class="margin-form">'
    .'<textarea name="brands" cols="48" rows="4">'.$brands.'</textarea>'
    .'</div><br/>'
    .'<label>'.$this->l('List families to determine models :').' </label>'
    .'<div class="margin-form">'
    .'<textarea name="families" cols="48" rows="4">'.$families.'</textarea>'
    .'</div><br/>'
    .'<label>'.$this->l('List of keywords for composed tags with brand, family and model :').' </label>'
    .'<div class="margin-form">'
    .'<textarea name="composedtags" cols="48" rows="4">'.$comptags.'</textarea>'
    .'</div><br/>'
    .'<label>'.$this->l('Meta title pattern :').' </label>'
    .'<div class="margin-form">'
    .'<input type="text" name="meta_title_pattern" size="44" value="'.$meta_title_pattern.'" />'
    .'</div><br/>'
    		.'<input type="submit" name="confRefMan" value="'.$this->l('Submit').'" />'
    .'</fieldset>'
    
    .'<fieldset class="width4"><legend><img src="'._PS_IMG_.'t/'.$this->name.'.gif" />'.$this->l('Images configuration').'</legend>'
    .'<label>'.$this->l('Number of fields in form :').'</label>'
    .'<div class="margin-form">'
    .'<input type="text" name="numfields" value="'.$numfields.'" size="3" />'
    .'</div><br/>'
    .'<input type="submit" name="confRefMan" value="'.$this->l('Submit').'" />'
    .'</fieldset>'
    
    .'</form></center>';
    
  }
  
  private function _postProcess() {
    if(Tools::isSubmit('confRefMan')) {
      $success = true;
      $dbfield = Tools::getValue('dbfield','reference');
      $excwords = Tools::getValue('excwords','');
      $brands = Tools::getValue('brands','');
      $families = Tools::getValue('families','');
      $comptags = Tools::getValue('composedtags','');
      $numfields = Tools::getValue('numfields','3');
      $meta_title_pattern = Tools::getValue('meta_title_pattern','');
            
      if(($dbfield=='supplier_reference') || ($dbfield=='reference'))
        Configuration::updateValue($this->name.'_dbfield',$dbfield,false);
      else
        Configuration::updateValue($this->name.'_dbfield','reference',false);

      Configuration::updateValue($this->name.'_excludewords',$excwords,false);
      Configuration::updateValue($this->name.'_brands',$brands,false);
      Configuration::updateValue($this->name.'_families',$families,false);
      Configuration::updateValue($this->name.'_composedtags',$comptags,false);
      Configuration::updateValue($this->name.'_numfields',$numfields,false);
      Configuration::updateValue($this->name.'_meta_title_pattern',$meta_title_pattern,false);
      
      if(!$success)
        $this->_html .= '<div class="alert error">'.$this->l('Cannot update settings').'</div>';
      else
        $this->_html .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="'.$this->l('Confirmation').'" />'.$this->l('Settings updated').'</div>';
    }
  }
}
?>