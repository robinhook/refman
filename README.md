Module for Prestashop 1.4/1.5 to quickly manage meta, tags, images and descriptions of products, based on their references or suppliers references.
Useful for products being parts of many differents items.

Version 0.2

In order to install, you must copy  all files to your prestashop module's directory in the subfolder refman (ie ..../prestashop/module/refman/).