<?php
if(!include_once('../../config/config.inc.php')) die('Include "config" manquant!');

include_once(dirname(__FILE__).'/refman.php');

$resp = array();
$mod = Module::getInstanceByName('refman');

$mod->context = Context::getContext();

$id_lang = 2;
$iso = Language::getIsoById($id_lang);

$sql = 'SELECT id FROM '._DB_PREFIX_.'crosslinks ORDER BY id DESC LIMIT 1;';
$hi_id_link = $mod->db->executeS($sql);
$hi_id_link = isset($hi_id_link[0])?intval($hi_id_link[0]['id']):1;

header('Content-type: application/javascript;');
?>
var strDelete = '<?php echo $mod->l('Delete'); ?>';
var strProductIdLinked = '<?php echo $mod->l('Product link id',null,$mod->context->language->id); ?>';
var strPropertyName = '<?php echo $mod->l('Property name',null,$mod->context->language->id); ?>';

var orefman = {
  adminDir:'',
  baseUri: '',
  higher_id_link: <?php echo $hi_id_link; ?>,
	active_tabid: 1,
	order: {t1:'id_product'},
	by: {t1:'ASC'},
	lastParms:{parm1:'',parm2:'',parm3:'',parm4:''},
	
//Descriptions
	checkAllDescs: function () {
		var checked = $('input[name=checkAllDescs]')[0].checked;
	  var objs = $('[id^=prodchosen-]');
	  for(var i = 0; i < objs.length; i++) {
	    objs[i].checked = checked;
	  }
	},
	
	blurTextArea: function (obj,type) {
	  switch(type) {
	    case 'parm1':
	      if(obj.value.length > 0)
	        orefman.lastParms.parm1 = obj.value;
	    break;
	    case 'parm2':
	      if(obj.value.length > 0)
	        orefman.lastParms.parm2 = obj.value
	    break;
	    case 'parm3':
	      if(obj.value.length > 0)
	        orefman.lastParms.parm3 = obj.value
	    break;
	    case 'parm4':
	      if(obj.value.length > 0)
	        orefman.lastParms.parm4 = obj.value
	    break;
	  }
	},
		
	changeVisibleDesc: function (id,force) {
	  if(force == undefined)
	    force = -1;
	  var dobj = $('#desc-'+id);
	  if(dobj.length > 0) {
	    if((dobj.css('display') == 'none') && (force != false) || (force == true))
	      dobj.css('display','table-row')
	      else dobj.css('display','none');
	  }
  },
  
//Cross Links
	deleteCrossLink: function (id_link) {
	  var obj = $('#link_'+id_link);
	  if(obj.length > 0) {
	    obj[0].parentNode.removeChild(obj[0]);
	    return true;
	  }
	  return false;
  },
	
	addCrossLink: function (id_product) {
	  orefman.higher_id_link ++;
	  var obj = $('#product_'+id_product);
	  var div = document.createElement('div');
	  div.id = 'link_'+orefman.higher_id_link;
	  div.className = 'newCLLine';
	  div.innerHTML = '<input type="hidden" name="id_link['+orefman.higher_id_link+']" value="'+orefman.higher_id_link+'" />'
	      +'<input type="hidden" name="id_product_src['+orefman.higher_id_link+']" value="'+id_product+'" />'
	      +strProductIdLinked+'<input size="8" type="text" name="id_product_dst['+orefman.higher_id_link+']" value="" />'
	      +strPropertyName+'<input list="references4" type="text" name="value['+orefman.higher_id_link+']" value="" />'
	      +'<input type="button" name="deletion" value="'+strDelete+'" onclick="orefman.deleteCrossLink('+orefman.higher_id_link+');" />'
	      +'<br/>';
	  if(obj.length > 0) {
	    obj[0].appendChild(div);
	    return true;
	  }
	  return false;
	},
		
//General
	selectedTabChange: function (id_tab) {
		  $(".menuTabButton.selected").removeClass("selected");
		  $("#menuTab"+id_tab).addClass("selected");
		  $(".tabItem.selected").removeClass("selected");
		  $("#menuTab"+id_tab+"Sheet").addClass("selected");
	},

	initTabs: function (tab_id) {
		  $(".menuTabButton").click(function () {
			  $(".menuTabButton.selected").removeClass("selected");
			  $(this).addClass("selected");
			  $(".tabItem.selected").removeClass("selected");
			  $("#" + this.id + "Sheet").addClass("selected");
		  });
		  orefman.selectedTabChange(orefman.active_tabid);
	},
	
	tinyMCEInit: function () {
      tinyMCE.init({
		mode : "textareas",
		theme : "advanced",
		editor_selector : "rte",
		plugins : "safari,pagebreak,style,table,advimage,advlink,inlinepopups,media,contextmenu,paste,fullscreen,xhtmlxtras,preview",
		// Theme options
		theme_advanced_buttons1 : "cut,copy,paste,pastetext,pasteword,|,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect",
		theme_advanced_buttons3 : ",search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,,|,forecolor,backcolor",
		theme_advanced_buttons4 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,media",
		theme_advanced_buttons5 : "styleprops,|,cite,abbr,acronym,del,ins,attribs,pagebreak,|,fullscreen",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,
        content_css : pathCSS+"global.css",
		document_base_url : ad,
		width: "488",
		height: "320",
		font_size_style_values : "8pt, 10pt, 12pt, 14pt, 18pt, 24pt, 36pt",
		elements : "nourlconvert,ajaxfilemanager",
		file_browser_callback : "ajaxfilemanager",
		entity_encoding: "raw",
		convert_urls : false,
        language : iso
	});
    },
	
	
	init: function () {
		reg = /.+[&;]tab_id=([0-9]){1}/gm;
		var tab_id = reg.exec(location.search);
		if(tab_id)
		  this.active_tabid = tab_id[1]
		  else this.active_tabid = 1;
		orefman.initTabs();
		orefman.tinyMCEInit();
	}
}

$(document).ready(function() {
  orefman.init();
});